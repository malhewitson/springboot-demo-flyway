create schema if not exists asgard;

USE asgard;

CREATE TABLE `file_loads`
(
    `id`              BIGINT(20)   NOT NULL AUTO_INCREMENT,
    `open_timestamp`  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `file_name`       VARCHAR(255),
    `file_type`       VARCHAR(20),
    `error_file_name` VARCHAR(255),
    `load_status`     VARCHAR(12),
    `processed_rows`  INT(11)               DEFAULT NULL,
    `error_rows`      INT(11)               DEFAULT NULL,
    `total_rows`      INT(11)               DEFAULT NULL,
    `s3_archive_path` VARCHAR(255) NULL     DEFAULT NULL,
    `s3_etag`         VARCHAR(40)  NULL     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ;

