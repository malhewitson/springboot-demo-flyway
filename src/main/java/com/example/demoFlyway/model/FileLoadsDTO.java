package com.example.demoFlyway.model;

import com.example.demoFlyway.jpa.FileLoadsEntity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * DTO for storing data from asgard.file_loads
 * table. Also has a field to store the content
 * of a csv file.
 */

public class FileLoadsDTO {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd.HHmmss");

    private Long id;
    private LocalDate openTimestamp;
    private String fileName;
    private String fileType;
    private String errorFileName;
    private String loadStatus;
    private Integer processedRows;
    private Integer errorRows;
    private Integer totalRows;
    private String s3ArchivePath;
    private String s3Etag;

    public FileLoadsDTO() {
    }


    public FileLoadsDTO(FileLoadsEntity entity) {
        this.id = entity.getId();
        this.fileName = entity.getFileName();
        this.fileType = entity.getFileType();
        this.errorFileName = entity.getErrorFileName();
        this.loadStatus = entity.getLoadStatus();
        this.processedRows = entity.getProcessedRows();
        this.errorRows = entity.getErrorRows();
        this.totalRows = entity.getTotalRows();
        this.s3ArchivePath = entity.getS3ArchivePath();
        this.s3Etag = entity.getS3Etag();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getOpenTimestamp() {
        return openTimestamp;
    }

    public void setOpenTimestamp(LocalDate openTimestamp) {
        this.openTimestamp = openTimestamp;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getErrorFileName() {
        return errorFileName;
    }

    public void setErrorFileName(String errorFileName) {
        this.errorFileName = errorFileName;
    }

    public String getLoadStatus() {
        return loadStatus;
    }

    public void setLoadStatus(String loadStatus) {
        this.loadStatus = loadStatus;
    }

    public Integer getProcessedRows() {
        return processedRows;
    }

    public void setProcessedRows(Integer processedRows) {
        this.processedRows = processedRows;
    }

    public Integer getErrorRows() {
        return errorRows;
    }

    public void setErrorRows(Integer errorRows) {
        this.errorRows = errorRows;
    }

    public Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }

    public String getS3ArchivePath() {
        return s3ArchivePath;
    }

    public void setS3ArchivePath(String s3ArchivePath) {
        this.s3ArchivePath = s3ArchivePath;
    }

    public String getS3Etag() {
        return s3Etag;
    }

    public void setS3Etag(String s3Etag) {
        this.s3Etag = s3Etag;
    }

}
