package com.example.demoFlyway.jpa;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "file_loads", schema = "asgard", catalog = "")
public class FileLoadsEntity {
    private long id;
    private String fileName;
    private String fileType;
    private String errorFileName;
    private String loadStatus;
    private Integer processedRows;
    private Integer errorRows;
    private Integer totalRows;
    private String s3ArchivePath;
    private String s3Etag;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "file_name")
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Basic
    @Column(name = "file_type")
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    @Basic
    @Column(name = "error_file_name")
    public String getErrorFileName() {
        return errorFileName;
    }

    public void setErrorFileName(String errorFileName) {
        this.errorFileName = errorFileName;
    }

    @Basic
    @Column(name = "load_status")
    public String getLoadStatus() {
        return loadStatus;
    }

    public void setLoadStatus(String loadStatus) {
        this.loadStatus = loadStatus;
    }

    @Basic
    @Column(name = "processed_rows")
    public Integer getProcessedRows() {
        return processedRows;
    }

    public void setProcessedRows(Integer processedRows) {
        this.processedRows = processedRows;
    }

    @Basic
    @Column(name = "error_rows")
    public Integer getErrorRows() {
        return errorRows;
    }

    public void setErrorRows(Integer errorRows) {
        this.errorRows = errorRows;
    }

    @Basic
    @Column(name = "total_rows")
    public Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }

    @Basic
    @Column(name = "s3_archive_path")
    public String getS3ArchivePath() {
        return s3ArchivePath;
    }

    public void setS3ArchivePath(String s3ArchivePath) {
        this.s3ArchivePath = s3ArchivePath;
    }

    @Basic
    @Column(name = "s3_etag")
    public String getS3Etag() {
        return s3Etag;
    }

    public void setS3Etag(String s3Etag) {
        this.s3Etag = s3Etag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileLoadsEntity that = (FileLoadsEntity) o;
        return id == that.id &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(fileType, that.fileType) &&
                Objects.equals(errorFileName, that.errorFileName) &&
                Objects.equals(loadStatus, that.loadStatus) &&
                Objects.equals(processedRows, that.processedRows) &&
                Objects.equals(errorRows, that.errorRows) &&
                Objects.equals(totalRows, that.totalRows) &&
                Objects.equals(s3ArchivePath, that.s3ArchivePath) &&
                Objects.equals(s3Etag, that.s3Etag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fileName, fileType, errorFileName, loadStatus, processedRows, errorRows, totalRows, s3ArchivePath, s3Etag);
    }
}
