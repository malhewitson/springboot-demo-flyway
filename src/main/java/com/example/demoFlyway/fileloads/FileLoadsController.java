package com.example.demoFlyway.fileloads;

import com.example.demoFlyway.model.FileLoadsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 *
 */

@Controller
public class FileLoadsController {


    @Autowired
    private FileLoadsService JPAservice;

    /**
     * get all file loads
     *
     * @return Result
     */
    @GetMapping("/demo/fileloads")
    @ResponseBody
    public List<FileLoadsDTO> getJPAAllFileLoads() {
        return JPAservice.getFileLoads();
    }

    /**
     *
     * @param fileLoadsDTO
     * @return ResponseEntity
     */
    @PostMapping("/demo/fileload")
    public ResponseEntity<Map<String, Long>> postJPAFileLoads(@RequestBody FileLoadsDTO fileLoadsDTO) {
        Long id = JPAservice.add(fileLoadsDTO);

        Map<String, Long> body = Collections.singletonMap("id", id);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.LOCATION, "/fileloads/" + id );

        return new ResponseEntity<>(body, httpHeaders, HttpStatus.CREATED);
    }



}
